<?php

namespace Rudashi\GoogleServices\Repository;

use Rudashi\GoogleServices\QueryBuilder;
use Rudashi\GoogleServices\Repository\Contract\Google;

/**
 * Class GoogleMail
 *
 * @package Rudashi\GoogleServices\Repository
 *
 * @property \Google_Service_Gmail $service
 */
class GoogleMail extends Google
{
    /**
     * @var array
     */
    protected $scopes = [
        \Google_Service_Gmail::GMAIL_READONLY,
        \Google_Service_Gmail::GMAIL_MODIFY,
    ];

    /**
     * @var string
     */
    protected $google_service = 'Google_Service_Gmail';

    /**
     * @var array
     */
    private $options = [];

    private $labels = [];

    public function setQuery(\Closure $queryBuilder) : GoogleMail
    {
        $query = $queryBuilder(new QueryBuilder)->make();
        array_map(function($value, $key) {
            $this->setOptions($key, $value);
        }, $query, array_keys($query) );

        return $this;
    }

    public function setOptions(string $key, $value) : GoogleMail
    {
        $this->options[$key] = $value;

        return $this;
    }

    /**
     * @return  \Google_Service_Gmail_Message[]
     */
    private function getMessages() : array
    {
        $messages = [];

        foreach ($this->service->users_messages->listUsersMessages($this->getServiceUserEmail(), $this->options)->getMessages() as $message) {
            $messages[] = $this->service->users_messages->get($this->getServiceUserEmail(), $message->getId());
        }
        return $messages;
    }

    /**
     * @param   \Closure $queryBuilder
     * @return  \Google_Service_Gmail_Message[]
     */
    public function messages(\Closure $queryBuilder) : array
    {
        $this->setQuery($queryBuilder);

        return $this->getMessages();
    }

    public function labels(\Closure $labels)
    {
        $labelService = new GoogleLabels($this->service, $this->getServiceUserEmail());

        if (empty($this->labels)) {
            $this->labels = $labelService->getLabels();
        }
        $labelService->setLabels($this->labels);
        return $labels($labelService);
    }

}