<?php

namespace Rudashi\GoogleServices\Repository;

use Rudashi\GoogleServices\Repository\Contract\Google;

/**
 * Class GoogleUserAlias
 *
 * @package Rudashi\GoogleServices\Repository
 *
 * @property \Google_Service_Directory $service
 */
class GoogleUserAlias extends Google
{

    /**
     * @var array
     */
    protected $scopes = [
        \Google_Service_Directory::ADMIN_DIRECTORY_USER
    ];

    /**
     * @var \Google_Service_Directory
     */
    protected $google_service = 'Google_Service_Directory';

    /**
     * @param   string  $userEmail
     * @return  \Illuminate\Support\Collection|\Illuminate\Support\MessageBag
     */
    public function getAliases(string $userEmail = '')
    {
        $data = collect([
            'primaryEmail' => $userEmail,
            'isAlias' => false,
            'aliases' => [],
        ]);

        try {
            $aliases = $this->service->users_aliases->listUsersAliases($userEmail)->getAliases();

            if ($aliases === null) {
                return $data;
            }

            if (!\in_array($userEmail, array_column($aliases, 'primaryEmail'), true)) {
                return $data->merge([
                    'primaryEmail' => $aliases[0]['primaryEmail'],
                    'isAlias' => true,
                ]);
            }

            return $data->merge([ 'aliases' => array_map(function($item) {
                return $item['alias'];
            }, $aliases)
            ]);

        } catch (\Exception $exception) {
            return $this->error(400, __('Google: '. json_decode($exception->getMessage())->error->message));
        }
    }

    /**
     * @param   string $alias
     * @param   string $userEmail
     * @return  \Illuminate\Support\Collection|\Illuminate\Support\MessageBag
     */
    public function addAlias(string $alias, string $userEmail)
    {
        try {
            $aliasObj  = new \Google_Service_Directory_Alias();
            $aliasObj->setAlias($alias);

            return collect([
                'alias' => $this->service->users_aliases->insert($userEmail, $aliasObj)->getAlias()
            ]);

        } catch (\Exception $exception) {
            return $this->error(400, __('Google: '. json_decode($exception->getMessage())->error->message));
        }
    }

    /**
     * @param   string $alias
     * @param   string $userEmail
     * @return  \Illuminate\Support\Collection|\Illuminate\Support\MessageBag
     */
    public function deleteAlias(string $alias, string $userEmail)
    {
        try {
            $this->service->users_aliases->delete($userEmail, $alias);

            return collect([
                'alias' => ''
            ]);

        } catch (\Exception $exception) {
            return $this->error(400, __('Google: '. json_decode($exception->getMessage())->error->message));
        }
    }

    /**
     * @param string $alias
     * @param string $newAliasOwnerEmail
     * @return \Illuminate\Support\Collection|\Illuminate\Support\MessageBag
     */
    public function changeAliasOwner(string $alias, string $newAliasOwnerEmail)
    {
        try {
            $this->deleteAlias($alias, $alias);
            return $this->addAlias($alias, $newAliasOwnerEmail);
        } catch (\Exception $exception) {
            return $this->error(400, __('Google: '. json_decode($exception->getMessage())->error->message));
        }
    }

}