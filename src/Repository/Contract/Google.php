<?php

namespace Rudashi\GoogleServices\Repository\Contract;

use Rudashi\GoogleServices\Client;

/**
 * Class Google
 * @package Rudashi\GoogleServices\Repository\Contract
 */
abstract class Google extends Client
{
    /**
     * @var array
     */
    protected $scopes = [];

    /**
     * @var string
     */
    protected $google_service;

    /**
     * @var \Google_Service
     */
    protected $service;

    /**
     * GoogleUserAlias constructor.
     *
     * @param   string          $serviceUserEmail
     * @throws  \Exception
     */
    public function __construct(string $serviceUserEmail = null)
    {
        parent::__construct($serviceUserEmail);
        $this->setScopes($this->scopes);
        $this->setService();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setService()
    {
        return $this->service = $this->make($this->google_service);
    }

}