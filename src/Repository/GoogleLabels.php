<?php

namespace Rudashi\GoogleServices\Repository;

class GoogleLabels
{
    /**
     * @var \Google_Service_Gmail
     */
    private $service;

    /**
     * @var string
     */
    private $clientEmail;

    /**
     * @var \Google_Service_Gmail_Label[]
     */
    private $labels;

    public function __construct(\Google_Service_Gmail $service, string $clientEmail)
    {
        $this->service = $service;
        $this->clientEmail = $clientEmail;
    }

    /**
     * @return  \Google_Service_Gmail_Label[]
     */
    public function getLabels() : array
    {
        $labels = [];

        $labelsResponse = $this->service->users_labels->listUsersLabels($this->clientEmail)->getLabels();

        if ($labelsResponse) {
            $labels += $labelsResponse;
        }

        return $labels;
    }

    public function setLabels(array $labels) : GoogleLabels
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * @param   string  $name
     * @return  \Google_Service_Gmail_Label|null
     */
    public function getByName(string $name)
    {
        $labels = $this->labels;
        foreach ($labels as $label) {
            if ($name === $label->getName()) {
                return $label;
            }
        }
        return null;
    }

    public function getByNames(array $names) : array
    {
        $labels = [];

        foreach ($names as $name) {
            $labels[] = $this->getByName($name);
        }
        return array_filter($labels);
    }

    /**
     * @param \Google_Service_Gmail_Label[] $labels
     * @return array
     */
    protected function getLabelsId(array $labels) : array
    {
        return array_map(function(\Google_Service_Gmail_Label $item) {
            return $item->getId();
        }, $labels );
    }

    public function add(string $messageId, array $labels)
    {
        $this->assignLabels($messageId, $labels);
    }

    public function remove(string $messageId, array $labels)
    {
        $this->assignLabels($messageId, [], $labels);
    }

    public function assignLabels(string $messageId, array $labelsToAdd = [], array $labelsToRemove = [])
    {
        $mods = new \Google_Service_Gmail_ModifyMessageRequest();
        if (!empty($labelsToAdd)) {
            $mods->setAddLabelIds($this->getLabelsId($this->getByNames($labelsToAdd)));
        }
        if (!empty($labelsToRemove)) {
            $mods->setRemoveLabelIds($this->getLabelsId($this->getByNames($labelsToRemove)));
        }
        $this->service->users_messages->modify($this->clientEmail, $messageId, $mods);
    }
}