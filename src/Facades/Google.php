<?php

namespace Rudashi\GoogleServices\Facades;

use Illuminate\Support\Facades\Facade;
use Rudashi\GoogleServices\Client;

class Google extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() : string
    {
        return Client::class;
    }
}