<?php

namespace Rudashi\GoogleServices;

class QueryBuilder
{
    private $query = [];

    private $options = [];

    public function getQuery() : array
    {
        return $this->query;
    }

    protected function setQuery(string $query) : QueryBuilder
    {
        $this->query[] = $query;

        return $this;
    }

    public function getOptions() : array
    {
        return $this->options;
    }

    protected function setOptions() : QueryBuilder
    {
        return $this->raw(implode(' ', $this->query));
    }

    public function make() : array
    {
        return $this->setOptions()->getOptions();
    }

    public function raw(string $query) : QueryBuilder
    {
        $this->options['q'] = $query;

        return $this;
    }

    protected function not(string $query) : QueryBuilder
    {
        return $this->setQuery("-$query");
    }

    public function inChat() : QueryBuilder
    {
        return $this->setQuery('in:chats');
    }

    public function notInChat() : QueryBuilder
    {
        return $this->not('in:chats');
    }

    public function from(string $email): QueryBuilder
    {
        return $this->setQuery("from:$email");
    }

    public function notFrom(string $email): QueryBuilder
    {
        return $this->not("from:$email");
    }

    public function newer(string $day) : QueryBuilder
    {
        return $this->setQuery("newer_than:{$day}d");
    }

    public function older(string $day) : QueryBuilder
    {
        return $this->setQuery("older_than:{$day}d");
    }

    public function before(string $date) : QueryBuilder
    {
        return $this->setQuery("before:{$date}");
    }

    public function after(string $date) : QueryBuilder
    {
        return $this->setQuery("after:{$date}");
    }

    public function label(string $label) : QueryBuilder
    {
        return $this->setQuery("label:{$label}");
    }

    public function notLabel(string $label) : QueryBuilder
    {
        return $this->not("label:{$label}");
    }

    public function notInLabels(array $labels) : QueryBuilder
    {
        return $this->not('label:{' . implode(', ', $labels).'}');
    }

    public function labelIn(array $labels) : QueryBuilder
    {
        return $this->setQuery('label:{' . implode(', ', $labels).'}');
    }
}