<?php

namespace Rudashi\GoogleServices;

use \Google_Client;
use Rudashi\GoogleServices\Exceptions\UnknownServiceException;
use Illuminate\Support\MessageBag;

class Client
{

    /**
     * @var \Google_Client
     */
    protected $client;

    /**
     * @var array
     */
    private $token;

    /**
     * @var string
     */
    private $serviceUserEmail;

    /**
     * @param   string $serviceUserEmail
     * @throws  \InvalidArgumentException
     */
    public function __construct($serviceUserEmail = null)
    {
        if ($serviceUserEmail !== null) {
            $this->setServiceUserEmail($serviceUserEmail);
        }

        // create an instance of the google client for OAuth2
        $this->client = new Google_Client(config('google-api.config'));

        // set application name
        $this->client->setApplicationName(config('google-api.application_name'));

        // set oauth2 configs
        $this->client->setClientId(config('google-api.client_id'));
        $this->client->setClientSecret(config('google-api.client_secret'));
        $this->client->setRedirectUri(env('APP_URL') . config('google-api.redirect_uri'));
        $this->client->setScopes(config('google-api.scopes'));
        $this->client->setAccessType(config('google-api.access_type'));
        $this->client->setApprovalPrompt(config('google-api.approval_prompt'));

        // set developer key
        $this->client->setDeveloperKey(config('google-api.developer_key'));

        // auth for service account
        if ($this->serviceUserEmail !== null && config('google-api.service.enable')) {

            $this->authService($this->serviceUserEmail);
        } else {
            $this->token = $this->getSavedToken();

            $this->client->setAccessToken($this->token);

            if ($this->client->isAccessTokenExpired() && $this->isRefreshTokenExists()) {
                $this->token = $this->generateRefreshToken($this->token['refresh_token']);
            }
        }
    }

    /**
     * Getter for the google client.
     *
     * @return \Google_Client
     */
    public function getClient() : Google_Client
    {
        return $this->client;
    }

    /**
     * Setter for the google client.
     *
     * @param \Google_Client $client
     * @return Client
     */
    public function setClient(Google_Client $client) : Client
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Getter for Token.
     *
     * @return array
     */
    public function getToken() : array
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getServiceUserEmail() : string
    {
        return $this->serviceUserEmail;
    }

    /**
     * @param   string $serviceUserEmail
     * @return  Client
     */
    public function setServiceUserEmail(string $serviceUserEmail) : Client
    {
        $this->serviceUserEmail = $serviceUserEmail;

        return $this;
    }

    /**
     * @param   array   $scopes
     * @return  Client
     */
    public function setScopes(array $scopes) : Client
    {
        $this->client->setScopes($scopes);

        return $this;
    }

    /**
     * Getter for the google service.
     *
     * @param string $service
     * @return object
     * @throws \Exception
     */
    public function make(string $service)
    {
        if (class_exists($service)) {
            $class = new \ReflectionClass($service);
            return $class->newInstance($this->client);
        }
        throw new UnknownServiceException($service);
    }

    /**
     * Setup correct auth method based on type.
     *
     * @param   string $userEmail
     */
    protected function authService(string $userEmail = '')
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.config('google-api.service.file'));

        // fallback to compute engine or app engine
        $this->client->useApplicationDefaultCredentials();
        $this->client->setSubject($userEmail);
    }

    /**
     *
     * @param string $filename
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getSavedToken($filename = 'gToken') : array
    {
        if (is_file(base_path($filename))) {
            return json_decode(file_get_contents(base_path($filename)), true);
        }

        throw new \InvalidArgumentException('File does not exist.');
    }

    /**
     * @return bool
     */
    public function isRefreshTokenExists() : bool
    {
        return isset($this->token['refresh_token']) ? true : false ;
    }

    /**
     * @param string $refreshToken
     * @param string $filename
     * @return array
     */
    public function generateRefreshToken(string $refreshToken, string $filename = 'gToken') : array
    {
        $token = $this->client->fetchAccessTokenWithRefreshToken($refreshToken);
        $file = base_path(isset($token['refresh_token']) ?  '/' : '/t') . $filename;

        file_put_contents($file, json_encode($token));

        return $token;
    }

    /**
     * Magic call method.
     *
     * @param   string $method
     * @param   array  $parameters
     * @return  mixed
     *
     * @throws  \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        if (method_exists($this->client, $method)) {
            return \call_user_func_array([$this->client, $method], $parameters);
        }
        throw new \BadMethodCallException(sprintf('Method [%s] does not exist.', $method));
    }

    /**
     * @param int $code
     * @param string $message
     * @return MessageBag
     */
    public function error(int $code, string $message) : MessageBag
    {
        return new  MessageBag([
            'code' => $code,
            'message' => $message

        ]);
    }
}