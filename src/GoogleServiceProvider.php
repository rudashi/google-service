<?php

namespace Rudashi\GoogleServices;

use Illuminate\Support\ServiceProvider;

class GoogleServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('google-api.php'),
        ], 'config');

        $this->loadViewsFrom(__DIR__.'/views', 'google-services');
    }

    /**
     * Register the application services.
     *
     * @return void
     * @throws \Exception
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/config.php', 'google-api');

        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->app->singleton(Client::class, function () {
            return new Client();
        });

        $this->app->make(GoogleController::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() : array
    {
        return [
            Client::class
        ];
    }
}
